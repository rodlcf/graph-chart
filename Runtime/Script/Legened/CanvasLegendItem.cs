﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ChartAndGraph.Legened
{
    /// <summary>
    /// use this in legened item prefabs to set an image and text for the legend item
    /// </summary>
    class CanvasLegendItem : MonoBehaviour
    {
        public Image Image = null;
        public TextMeshProUGUI Text = null;

		public CanvasLegend legend;

		public virtual void OnGenerate(LegenedData.LegenedItem item, int index)
		{
			if (Image != null)
			{
				if (item.Material == null)
					Image.material = null;
				else
				{
					if (isGradientShader(item.Material))
					{
						Image.material = CreateCanvasGradient(item.Material);
					}
					else
					{
						Image.material = null;
						Texture2D tex = item.Material.mainTexture as Texture2D;
						if (tex != null)
							Image.sprite = CreateSpriteFromTexture(tex);
						Image.color = item.Material.color;
					}
				}
			}
			if (Text != null)
			{
				Text.text = item.Name;
			}
		}

		bool isGradientShader(Material mat)
		{
			if (mat.HasProperty("_ColorFrom") && mat.HasProperty("_ColorTo"))
				return true;
			return false;
		}

		Sprite CreateSpriteFromTexture(Texture2D t)
		{
			Sprite sp = Sprite.Create(t, new Rect(0f, 0f, (float)t.width, (float)t.height), new Vector2(0.5f, 0.5f));
			sp.hideFlags = HideFlags.DontSave;
			legend.mToDispose.Add(sp);
			return sp;
		}

		Material CreateCanvasGradient(Material mat)
		{
			Material grad = new Material((Material)Resources.Load("Chart And Graph/Legend/CanvasGradient"));
			grad.hideFlags = HideFlags.DontSave;
			Color from = mat.GetColor("_ColorFrom");
			Color to = mat.GetColor("_ColorTo");
			grad.SetColor("_ColorFrom", from);
			grad.SetColor("_ColorTo", to);
			legend.mToDispose.Add(grad);
			return grad;
		}
	}
}
