﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ChartAndGraph.Legened
{
    /// <summary>
    /// class for canvas legned. this class basiically creates the legned prefab for each category in the chart
    /// </summary>
    [ExecuteInEditMode]
    class CanvasLegend : MonoBehaviour
    {
        [SerializeField]
        private int fontSize;

        public int FontSize
        {
            get { return fontSize; }
            set
            {
                fontSize = value;
                PropertyChanged();
            }
        }

        [SerializeField]
        private CanvasLegendItem legendItemPrefab;

        public CanvasLegendItem LegenedItemPrefab
        {
            get { return legendItemPrefab; }
            set
            {
                legendItemPrefab = value;
                PropertyChanged();
            }
        }

        [SerializeField]
        private AnyChart chart;

        public List<UnityEngine.Object> mToDispose = new List<UnityEngine.Object>();
        bool mGenerateNext = false;
        public AnyChart Chart
        {
            get { return chart; }
            set
            {
                if (chart != null)
                    ((IInternalUse)chart).Generated -= CanvasLegend_Generated;
                chart = value;
                if(chart != null)
                    ((IInternalUse)chart).Generated += CanvasLegend_Generated;
                PropertyChanged();
            }
        }
        void Start()
        {
            if (chart != null)
                ((IInternalUse)chart).Generated += CanvasLegend_Generated;
            InnerGenerate();
        }
        void OnEnable()
        {
            if (chart != null)
                ((IInternalUse)chart).Generated += CanvasLegend_Generated;
            InnerGenerate();
        }
        void OnDisable()
        {
            if (chart != null)
                ((IInternalUse)chart).Generated -= CanvasLegend_Generated;
        //    Clear();
        }
        void OnDestory()
        {
            if (chart != null)
                ((IInternalUse)chart).Generated -= CanvasLegend_Generated;
            Clear();
        }
        private void CanvasLegend_Generated()
        {
           InnerGenerate();
        }
        protected void OnValidate()
        {
            if (chart != null)
                ((IInternalUse)chart).Generated += CanvasLegend_Generated;
            Generate();
        }
        protected void PropertyChanged()
        {
            Generate();
        }

        public void Clear()
        {
            CanvasLegendItem[] items = gameObject.GetComponentsInChildren<CanvasLegendItem>();
            for(int i=0; i<items.Length; i++)
            {
                if (items[i] == null || items[i].gameObject == null)
                    continue;
                ChartCommon.SafeDestroy(items[i].gameObject);
            }
            for(int i=0; i<mToDispose.Count; i++)
            {
                UnityEngine.Object obj = mToDispose[i];
                if (obj != null)
                    ChartCommon.SafeDestroy(obj);
            }
            mToDispose.Clear();
        }

        public void Generate()
        {
            mGenerateNext = true;
        }

        void LateUpdate()
        {

            if (mGenerateNext == true)
                InnerGenerate();
        }

        private void InnerGenerate()
        {
            mGenerateNext = false;

            if (enabled == false || gameObject.activeInHierarchy == false)
                return;

            Clear();
            if (chart == null || legendItemPrefab == null)
                return;
            LegenedData inf = ((IInternalUse)chart).InternalLegendInfo;
            if (inf == null)
                return;

			int index = 0;
            foreach(LegenedData.LegenedItem item in inf.Items)
            {
                GameObject prefab =  (GameObject)GameObject.Instantiate(legendItemPrefab.gameObject);
                prefab.transform.SetParent(transform, false);
                ChartCommon.HideObject(prefab, true);
                CanvasLegendItem legendItemData = prefab.GetComponent<CanvasLegendItem>();

				legendItemData.legend = this;
				legendItemData.OnGenerate(item, index);

				index++;
            }
        }
    }
}
